//
//  SecondViewController.swift
//  DiceRoll
//
//  Created by Nurba on 16.04.2021.
//

import UIKit



class SecondViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
   
    var firstLab : Int?
    var secondLab : Int?
    public var rolls: [(first: Int , second: Int)] = [(Int, Int)]()
   

    var first: Int = 0
    var second: Int = 0
    
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self

        tableView.register(UINib(nibName: RollCell.identifier, bundle: Bundle(for: RollCell.self)), forCellReuseIdentifier: RollCell.identifier)
  
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if rolls.count != 0{
            tableView.reloadData()
        }
    }
    @IBAction func pressedBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    


}
extension SecondViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rolls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RollCell.identifier, for: indexPath) as! RollCell
        let first = rolls[indexPath.row].first + 1
        let second = rolls[indexPath.row].second + 1
        cell.firstLabel.text = "First: \(first)"
        cell.secondLabel.text = "Second: \(second)"

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        dismiss(animated: true, completion: nil)
    }
    
    
}

