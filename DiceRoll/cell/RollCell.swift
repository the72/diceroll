//
//  RollCell.swift
//  DiceRoll
//
//  Created by Nurba on 16.04.2021.
//

import UIKit

class RollCell: UITableViewCell {
    public static let identifier = "RollCell"
    
    var delegate:ViewController!
    
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    
    }

    
}
